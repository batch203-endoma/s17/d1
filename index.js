console.log("Hello World");

// [SECTION] Functions


// Functiun declarations
// function statement defines a function with the specified parameters.
/*
  function functionName(){
  code block (statement)
}

function keyword - use to define a javascript function.

function code block ({}) - the statements which comprise the body of the fucntion. This is where the code to be executed.
*/

function printName() {
  console.log("My name is John");
}


// [SECTION] Function Invocation
// It is common to use the term "Call a function" instead of "invoke a function".
// Let's invoke/call the function that was declared.

printName();

// results in an error, much like variables, we cannot invoke a function that we have not define yet.

function declaredFunction() {
  console.log("Hello World")
}

declaredFunction();

// [Section] Function Declaration vs Expression

// Function declarations
  // A Function can be created through function declaration by using the fucntion keyword and adding a function name.

  //Function Expression
    // a function can be also stored in a variable. This is called as function expression.

    // A function expression is a an anonymous function assigned to the variableFunction
      //  anonymous function - a function w/o a printName


      let variableFunction = function() {
        console.log ("Hellow Again!");
      }

      variableFunction();

      let funcExpression = function funcName() {
        console.log("Hello from the other side.")
      }

    funcExpression();

    declaredFunction();

    declaredFunction = function () {
      console.log ("updated declaredFunction")
    }

    declaredFunction();


    functionExpression = function() {
      console.log ("updated funcExpression");
    }

    functionExpression();

    const constantFunc =function(){
      console.log("Initialized with const");
    }
    constantFunc();
    // However, we cannot re-assign a function expression initialized with const.
    // constantFunc = function(){
    // console.log("New Value");
    // }
    //
    // constantFunc();

    // [section] Function Scooping

    // scope is the accessibility/visibility of a variables in the code.

    // Javascript Variables has 3 types of scope:
    // 1. Locals/Block scope
    // 2. global scope
    // function scope

      // Variables declared inside a {} block can only be access locally.
      // Local and block scope only works with let and const.

    {
      let localVar = "Armando Perez";
      // var localVar = "Armando Perez";
      console.log(localVar);
    }
      let globalVar = "Mr. Worldwide";


    console.log(globalVar);
    // console.log(localVar);

    // FunctionScopes
      // Javascript has function scopes: each function creates a new scope.
      // Variables defined inside a function are not accessible/ visible outside the function.
      // Variables declared with var, let and const are quite similar when declared inside a function.
    function showNames(){
      // function scoped variables:
      var functionVar = "Joe";
      const functionConst = "John";
      let functionLet = "Jane";

      console.log(functionVar);
      console.log(functionConst);
      console.log(functionLet);
    }

    showNames();

    function myNewFunction(){
      let name = "Jane";
      function nestedFunction(){
        let nestedName ="John"
        console.log(name);
      }
          nestedFunction();
      // console.log(nestedName); result to not defined error.
    }

    myNewFunction();

    // Function and global scope Variables

    // Global scoped Variables
    let globalName ="Alexandro";
    function nmyNewFunction2() {
      let nameInside = "Renz";
      console.log(globalName);
    }

    nmyNewFunction2();




    // Error - This are function scoped variable and cannot be accessed outside the function.
    // console.log(functionVar);
    // console.log(functionConst);
    // console.log(functionLet);



  // Nested Functions
    // You can create another function inside a function.
    // this is called a nested function.


    function showSampleAlter(){
      alter("Hello, User!");
    }

    //showSampleAlert();
    console.log("I will only log in the console when the alert is dismissed.");



function printWelcomeMessage(){
  let firstName = prompt("Enter your first name: ");
  let lastName = prompt("Enter your last name: ");

  console.log("Hello, "+ firstName + " " +lastName+ "!");
  console.log("Welcome to my page!");
}

printWelcomeMessage();


// [SECTION] Function naming convetion

function getCourses(){
  let courses = ["Science 101", "Math 101", "English 101"];
  console.log(courses);
}
getCourses();

// Avoid generic names to avoid confusion within our code.

function get() {
  let name ="Jamie"
  console.log(name);
}

get();

// Avoid pointless and inappropriate function names, example: foo, bar

function foo(){
  console.log(25%5);
}
foo();


// Name your functions in small caps. follow camelCase when naming variables and functions.

function displayCarInfo() {
  console.log("Brand: Toyota");
  console.log("Type: Sedan");
  console.log("Price: 1,500,000");
}

displayCarInfo();









    //










//
